//
//  uberApp.swift
//  uber
//
//  Created by Alexey Khomych on 02.11.2023.
//

import SwiftUI

@main
struct uberApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
